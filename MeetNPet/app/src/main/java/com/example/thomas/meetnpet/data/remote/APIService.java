package com.example.thomas.meetnpet.data.remote;

import com.example.thomas.meetnpet.data.model.User;

import org.json.JSONArray;

import java.lang.reflect.Array;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {


    @POST("/api/users")
    @FormUrlEncoded
    Call<User> saveUser(
                        @Field("email") String email,
                        @Field("password") String password,
                        @Field("name") String name,
                        @Field("firstname") String firstname,
                        @Field("resetToken") String resetToken
                        );


    @Headers("Content-Type: application/json")
    @GET("/api/users")
    Call<User> getUser(

    );

//    @POST("/loginuser")
//    @FormUrlEncoded
//    Call<User> loginuser(
//            @Field("email") String email,
//            @Field("password") String password
//    );
@POST("/loginuser")
@FormUrlEncoded
Call<User> loginuser(
        @Field("email") String email,
        @Field("password") String password
);
    
}
