package com.example.thomas.meetnpet;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONObject;

import okhttp3.HttpUrl;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {

    static String TAG = "logApp";

    String email;
    String token;
    String emailSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Intent intent = getIntent();
        String email = intent.getStringExtra("email");
        String token = intent.getStringExtra("token");

        Button searchBtn = (Button) findViewById(R.id.searchButton);
        final EditText numberinput = (EditText) findViewById(R.id.numberInput);


        Button deleteButton = (Button) findViewById(R.id.deleteuser);

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String number = numberinput.getText().toString().trim();
                voidGetuser(number);
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String number = numberinput.getText().toString().trim();
                deleteUser(number);
            }

        });

    }

    public void deleteUser(String id){
        String URL = "https://www.api.restoklm.com/api/users/"+id+"/";
        IRetrofit jsonPostService = ServiceGenerator.createService(IRetrofit.class, URL);
        Call<JsonElement> call = jsonPostService.deleteuser();
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                Log.e(TAG,"USER DELETE");
                finish();
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                Log.e(TAG,"USER DELETE FAILLURE");
            }
        });
    }


    public void voidGetuser(String id){
        String URL = "https://www.api.restoklm.com/api/users/"+id+"/";
        IRetrofit jsonPostService = ServiceGenerator.createService(IRetrofit.class, URL);
        Call<JsonElement> call = jsonPostService.getuser();
        Log.e(TAG,call.request().toString());
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                TextView emailCurrentUserSelected = (TextView) findViewById(R.id.useremail);

                if (response.body() == null){
                    String messageE = "Aucune correspondance trouvée";
                    emailCurrentUserSelected.setText(messageE);
                }
                else{
                    JsonObject json = response.body().getAsJsonObject();
                    String mail= json.get("email").toString();
                    emailCurrentUserSelected.setText(mail);
                    Log.e(TAG,response.body().toString());
                }







            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                Log.e(TAG,"FAIL");
            }
        });


    }
}
