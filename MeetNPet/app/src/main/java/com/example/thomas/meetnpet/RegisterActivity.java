package com.example.thomas.meetnpet;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thomas.meetnpet.data.model.User;
import com.example.thomas.meetnpet.data.remote.APIService;
import com.example.thomas.meetnpet.data.remote.ApiUtils;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RegisterActivity extends AppCompatActivity {

    private TextView mResponseTV;
    private APIService mAPIService;


    JsonObject jsonObject = new JsonObject();


    String email;
    String password;

    static String TAG = "logApp";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Button registerBtn = (Button) findViewById(R.id.btn_submit);


        final EditText emailEt = (EditText) findViewById(R.id.email);
        final EditText passwordEt = (EditText) findViewById(R.id.password);


        mResponseTV = (TextView) findViewById(R.id.tv_response);

        mAPIService = ApiUtils.getAPIService();



        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = emailEt.getText().toString().trim();
                String password = passwordEt.getText().toString().trim();

                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("email",email);
                jsonObject.addProperty("plainPassword", password);
                jsonObject.addProperty("enabled", true);



                if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)){

                    sendJsonuser(jsonObject);
                    Log.e(TAG, "User{json objet : " + jsonObject +'\'' +
                            ", email='" + email + '\'' +
                            ", plainPassword='" + password + '\'' +

                            '}' );

                }
                else{
                    Toast.makeText(RegisterActivity.this, "Veuillez remplir tous les champs", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }




    public void sendJsonuser(JsonObject jsonObject){

        IRetrofit jsonPostService = ServiceGenerator.createService(IRetrofit.class, "https://www.api.restoklm.com/");
        Call<JsonObject> call = jsonPostService.postRawJSON(jsonObject);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.e(TAG,"user ok"+response.body());
                Toast.makeText(RegisterActivity.this, "Votre compte a bien été crée", Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(RegisterActivity.this, "Une erreur est survenue, veuillez réessayer ultérieurement", Toast.LENGTH_SHORT).show();
                Log.e(TAG, "user Denied" + t);
            }
        });
    }


//    public void sendUser(String name, String firstname, String email, String password){
//        mAPIService.saveUser(email,password,name,firstname,"vide").enqueue(new Callback<User>() {
//
//
//
//            @Override
//            public void onResponse(Call<User> call, Response<User> response) {
//                if (response.isSuccessful()){
//                    showResponse(response.body().toString());
//                    Log.e(TAG, "user submitted to API." + response.body().toString());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<User> call, Throwable t) {
//                Log.e(TAG, "Unable to submit user to API" + t);
//            }
//        });
//
//    }
    public void showResponse(String response){
        if (mResponseTV.getVisibility() == View.GONE){
            mResponseTV.setVisibility(View.VISIBLE);
        }
        mResponseTV.setText(response);
    }






}
