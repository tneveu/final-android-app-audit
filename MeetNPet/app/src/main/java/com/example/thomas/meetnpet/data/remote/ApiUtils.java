package com.example.thomas.meetnpet.data.remote;

public class ApiUtils {
    public static final String BASE_URL = "http://www.api.restoklm.com/";
    private ApiUtils(){}
    public static APIService getAPIService(){
        return RetrofitClient.getClient(BASE_URL).create(APIService.class);
    }
}
