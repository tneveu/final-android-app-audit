package com.example.thomas.meetnpet;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thomas.meetnpet.data.model.User;
import com.example.thomas.meetnpet.data.remote.APIService;
import com.example.thomas.meetnpet.data.remote.ApiUtils;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import org.json.JSONArray;

import java.io.FileReader;
import java.lang.reflect.Array;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {






    private TextView mResponseTV;
    private APIService mAPIService;




    JsonObject jsonObject = new JsonObject();

    String name;
    String firstname;
    String email;
    String password;
    String identifiant;


    static String TAG = "logApp";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button toregisterBtn = (Button) findViewById(R.id.btntoRegister);

        mResponseTV = (TextView) findViewById(R.id.tv_response);
        mAPIService = ApiUtils.getAPIService();

        final EditText identifiantET = (EditText) findViewById(R.id.identifiant);
        final EditText passwordET = (EditText) findViewById(R.id.password);
        Button loginBtn = (Button) findViewById(R.id.login);


        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String identifiant = identifiantET.getText().toString().trim();
                String password = passwordET.getText().toString().trim();
                Log.e(TAG,identifiant+password);

//                attention l'email est enfaite le username dans la BDD pour le login
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("email",identifiant);
                jsonObject.addProperty("password", password);







                if (!TextUtils.isEmpty(identifiant) && !TextUtils.isEmpty(password)){
                    loginUser(jsonObject);


                    Log.e(TAG, "Send object" + identifiant + password);
                }


            }
        });



        toregisterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, RegisterActivity.class);
                startActivity(i);
            }
        });





    }


    public void loginUser(JsonObject jsonObject){
        IRetrofit jsonPostService = ServiceGenerator.createService(IRetrofit.class, "https://www.api.restoklm.com/");
        Call<JsonObject> call = jsonPostService.login(jsonObject);
        final String email = jsonObject.get("email").toString();
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject jsonObject1 = (JsonObject) response.body();
                String token = jsonObject1.get("token").toString();
                Log.e(TAG,"user logged, token : "+token);
                Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                intent.putExtra("email",email);
                intent.putExtra("token", token);
                startActivity(intent);

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });


    }




    public void getUser(){
        mAPIService.getUser().enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Log.e(TAG, "user list ok ");
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.e(TAG, "user list fail ");
            }
        });
    }











}
