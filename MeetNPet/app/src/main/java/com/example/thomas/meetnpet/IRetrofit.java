package com.example.thomas.meetnpet;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import okhttp3.HttpUrl;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface IRetrofit {
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("/api/users")
    Call<JsonObject> postRawJSON(@Body JsonObject jsonObject);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("/login_check")
    Call<JsonObject> login(@Body JsonObject jsonObject);


    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @GET("?rien=")
    Call<JsonElement> getuser();


    @DELETE("?rien=")
    Call<JsonElement> deleteuser();



    @Headers("Content-Type: application/json")
    @POST("/loginuser")
    Call<JsonObject> loginJSON(@Body JsonPrimitive jsonPrimitive);
}

